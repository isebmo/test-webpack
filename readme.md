
Test project configuration with Webpack.
Inspired by https://www.sitepoint.com/beginners-guide-to-webpack-2-and-module-bundling/


# Project initialization
Execute following commands :
```
npm init -y
npm install webpack --save-dev
mkdir src
touch index.html src/app.js webpack.config.js
npm install css-loader style-loader sass-loader node-sass --save-dev
npm install postcss-loader autoprefixer --save-dev
```